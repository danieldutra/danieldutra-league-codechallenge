using Microsoft.AspNetCore.Mvc;

namespace ArrayManipulation2.Controllers
{
    [ApiController]
    [Route("/")]
    public class ArrayManipulationController : ControllerBase
    {
        const string newline = "\n";

        private readonly ILogger<ArrayManipulationController> _logger;

        public ArrayManipulationController(ILogger<ArrayManipulationController> logger)
        {
            _logger = logger;
        }

        [HttpPost("echo")]
        public IActionResult Echo(IFormFile csvFile)
        {
            var matrix = ReadCsvFile(csvFile);

            if (matrix == null)
                return BadRequest("Failed to parse csv file.");

            return Ok(string.Join(newline, matrix.Select(line => string.Join(",", line))));
        }

        [HttpPost("invert")]
        public IActionResult Invert(IFormFile csvFile)
        {
            var matrix = ReadCsvFile(csvFile);

            if (matrix == null)
                return BadRequest("Failed to parse csv file.");

            string invertedMatrix = "";
            for (int i = 0; i < matrix.First().Length; i++)
            {
                for (int j = 0; j < matrix.Length; j++)
                {
                    invertedMatrix += matrix[j][i].ToString() + ",";
                }
                invertedMatrix += newline;
            }
            return Ok(invertedMatrix);
        }

        [HttpPost("flatten")]
        public IActionResult Flatten(IFormFile csvFile)
        {
            var matrix = ReadCsvFile(csvFile);

            if (matrix == null)
                return BadRequest("Failed to parse csv file.");

            return Ok(string.Join(",", matrix.SelectMany(x => x)));
        }

        [HttpPost("sum")]
        public IActionResult Sum(IFormFile csvFile)
        {
            var matrix = ReadCsvFile(csvFile);

            if (matrix == null)
                return BadRequest("Failed to parse csv file.");

            return Ok(matrix.SelectMany(x => x).Aggregate((a, b) => a + b));
        }

        [HttpPost("multiply")]
        public IActionResult Multiply(IFormFile csvFile)
        {
            var matrix = ReadCsvFile(csvFile);

            if (matrix == null)
                return BadRequest("Failed to parse csv file.");

            return Ok(matrix.SelectMany(x => x).Aggregate((a, b) => a * b));
        }

        /// <summary>
        /// Parse a csv file content into a matrix
        /// </summary>
        /// <param name="csvFile">File with a square matrix values</param>
        /// <returns>Parsed csv content or null if fail to parse</returns>
        int[][] ReadCsvFile(IFormFile csvFile)
        {
            try
            {
                if (csvFile?.Length > 0)
                {
                    var csvRows = new List<string>();
                    using (var reader = new StreamReader(csvFile.OpenReadStream()))
                    {
                        while (reader?.Peek() >= 0)
                            csvRows.Add(reader?.ReadLine() ?? "");
                    }
                    return csvRows.Select(x => x.Split(',').Select(x => int.Parse(x)).ToArray()).ToArray();
                }
            }
            catch { _logger.LogWarning("Error while trying to parse user csv file"); }

            return null;
        }

    }
}
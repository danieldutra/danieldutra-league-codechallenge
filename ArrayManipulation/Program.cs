var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

var testMatrix = new[]
{
    new[] { 1, 2, 3 },
    new[] { 4, 5, 6 },
    new[] { 7, 8, 9 },
};

const string newline = "\n";

app.MapGet("/echo", (string? csvPath) =>
{
    var matrix = ReadCsvFile(csvPath);
    return string.Join(newline, matrix.Select(line => string.Join(",", line)));
});

app.MapGet("/invert", (string? csvPath) =>
{
    var matrix = ReadCsvFile(csvPath);

    string invertedMatrix = "";
    for (int i = 0; i < matrix.First().Length; i++)
    {
        for (int j = 0; j < matrix.Length; j++)
        {
            invertedMatrix += matrix[j][i].ToString() + ",";
        }
        invertedMatrix += newline;
    }
    return invertedMatrix;
});

app.MapGet("/flatten", (string? csvPath) =>
{
    var matrix = ReadCsvFile(csvPath);
    return string.Join(",", matrix.SelectMany(x => x));
});

app.MapGet("/sum", (string? csvPath) =>
{
    var matrix = ReadCsvFile(csvPath);
    return matrix.SelectMany(x => x).Aggregate((a, b) => a + b);
});

app.MapGet("/multiply", (string? csvPath) =>
{
    var matrix = ReadCsvFile(csvPath);
    return matrix.SelectMany(x => x).Aggregate((a, b) => a * b);
});

int[][] ReadCsvFile(string? csvPath)
{
    try
    {
        if (!string.IsNullOrEmpty(csvPath))
        {
            string[] csvRows = File.ReadAllLines(csvPath);
            return csvRows.Select(x => x.Split(',').Select(x => int.Parse(x)).ToArray()).ToArray();
        }
    }
    catch { /* handle exception */ }

    return testMatrix;
}

app.Run();